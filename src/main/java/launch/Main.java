package launch;


import control.Controller;
import model.MonitoredData;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;
import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String argv[]) throws ParseException {
        Controller control = new Controller();
        //read from file and return Stream<String>
        Stream<String> streamString = control.readFile("D:\\Projects\\IntelliJ\\PT2018_30226_Coroiu_Serban_Assignment_5\\src\\main\\resources\\Activity\\Activities.txt");
        //splitam stream-ul pentru a construi un stream de Monitored Data
        List<MonitoredData> data = control.convertMd(streamString);
        //data.forEach(System.out::println);

        System.out.println("\nCerinta 1:");

        int differentDays = control.task1(data);
        System.out.println(differentDays);

        System.out.println("\nCerinta 2:");
        Map<String, Long> maptask2 = new HashMap<>();
        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter("./cerinta2.txt"));
            maptask2 = control.task2(data);
            maptask2.forEach((activity, count) -> {
                try {
                    writer.write(activity+ " a fost realizata de  " +count+ " ori.\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Fisierul cerinta2.txt a fost scris!");

        System.out.println("\nCerinta 3:");


        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter("./cerinta3.txt"));
            Map<Integer, Map<String, Long>> task3 = control.task3(data);
            task3.forEach((day, map) -> {
                try {
                    writer.write("Ziua "+day+"\n");
                    map.forEach((activity, count) -> {
                        try {
                            writer.write("\tActivitatea "+activity+" a fost realizata de " + count+ " ori\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Fisierul cerinta3.txt a fost scris!");


        System.out.println("\nCerinta 4:");
        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter("./cerinta4.txt"));
            List<Duration> times = control.task4(data);
            times.stream().forEach(time ->{
                try {
                    writer.write(time+"\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Fisierul cerinta4.txt a fost scris!");

        System.out.println("\nCerinta 5:");

        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter("./cerinta5.txt"));
            Map<String, Duration> times = control.task5(data);
            times.forEach((action, time) -> {
                try {
                    writer.write(action +" a durat in total: "+time + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Fisierul cerinta5.txt a fost scris!");


        System.out.println("\nCerinta 6:");
        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter("./cerinta6.txt"));
            List<String> result = control.task6(data, maptask2);
            result.forEach(action -> {
                try {
                    writer.write(action + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Fisierul cerinta6.txt a fost scris!");
    }
}
