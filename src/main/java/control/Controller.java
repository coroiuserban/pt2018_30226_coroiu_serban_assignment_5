package control;

import model.MonitoredData;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Controller {
    public Controller(){

    }

    public Stream<String> readFile(String fileName){
        Stream<String> stream = null;
        try {
            stream = Files.lines(Paths.get(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream;
    }

    public List<MonitoredData> convertMd(Stream<String> stream){
        List<MonitoredData> data = stream.map(line ->{
            String[] splitLine = line.split("\t\t");
            MonitoredData md = new MonitoredData(splitLine[0], splitLine[1], splitLine[2]);
            return md;
        }).collect(Collectors.toList());
        return data;
    }

    public int task1(List<MonitoredData> data){
        return (int)data.stream().map(field-> field.getStartDateFormat().getDayOfMonth()).distinct().count();
    }

    public Map<String, Long> task2(List<MonitoredData> data){
        Map<String, Long> map = data.stream().collect(Collectors.groupingBy(field-> field.getActivity(), Collectors.counting()));
        return map;
    }

    public Map<Integer, Map<String, Long>> task3(List<MonitoredData> data){
        Map<Integer, Map<String, Long>> map3;
        map3 = data.stream().collect(Collectors.groupingBy( field->field.getStartDateFormat().getDayOfMonth(), Collectors.groupingBy(field->field.getActivity(), Collectors.counting() )));
        return map3 ;
    }

    public List<Duration> task4(List<MonitoredData> data){
        List<Duration> map4 = new ArrayList<>();
        data.stream().forEach(field ->{
            map4.add(getInterval(field.getEndDateFormat(), field.getStartDateFormat()));
        });
        return map4;
    }

    private Duration getInterval(LocalDateTime end, LocalDateTime start){
        return Duration.between(start.toLocalTime(), end.toLocalTime());
    }

    public Map<String, Duration> task5(List<MonitoredData> data){
       Map<String, Duration> times = new HashMap<>();
       data.stream().forEach(fields -> {
           Duration durata = Duration.between(fields.getStartDateFormat(), fields.getEndDateFormat());
           if(times.get(fields.getActivity()) != null)
                times.put(fields.getActivity(), times.get(fields.getActivity()).plus(durata));
           else
               times.put(fields.getActivity(), durata);
       });
       return times;
    }

    public List<String> task6(List<MonitoredData> data, Map<String, Long> mapTask2){
        List<String> result = new ArrayList<>();
        Map<String, Long> activitatiCandidate = data.stream().filter(act-> Duration.between(act.getStartDateFormat(), act.getEndDateFormat()).toMinutes() < 5).collect(Collectors.groupingBy(activity->activity.getActivity(),Collectors.counting()));
        activitatiCandidate.forEach((name, count) ->{
            double activityTotal = (double)mapTask2.get(name);
            if(count >= activityTotal*0.9)
                result.add(name);
        });
        return result;
    }

}
