package model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData {
    private String startTime;
    private String endTime;
    private String activity;

    public MonitoredData(){
        activity = "init";
    }

    public MonitoredData(String startTime, String endTime, String activity){
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public String getStartTime(){
        return startTime;
    }

    public String getEndTime(){
        return endTime;
    }

    public String getActivity(){
        return activity;
    }

    public void setStartTime(String startTime){
        this.startTime = startTime;
    }

    public void setEndTime(String endTime){
        this.endTime = endTime;
    }

    public void setActivity(String activity){
        this.activity = activity;
    }

    public String toString(){
        return "start: " + startTime + ", end: "+ endTime+ ", activity: "+activity;
    }

    public Integer getNO(){
        return (int)0;
    }
    public LocalDateTime getStartDateFormat(){
        DateTimeFormatter format=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startdate = LocalDateTime.parse(startTime, format);
        return startdate;
    }

    public LocalDateTime getEndDateFormat(){
        DateTimeFormatter format=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime enddate = LocalDateTime.parse(endTime, format);
        return enddate;
    }
}
